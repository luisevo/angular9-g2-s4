import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'spotify', pathMatch: 'full' },
  {
    path: 'spotify',
    loadChildren: () => import('./spotify/spotify.module').then(m => m.SpotifyModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
