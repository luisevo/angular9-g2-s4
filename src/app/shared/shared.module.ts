import { NgModule } from '@angular/core';
import { SharedComponentsModule } from './components/components.module';
import { SharedPipesModule } from './pipes/pipes.module';

@NgModule({
  exports: [
    SharedComponentsModule,
    SharedPipesModule
  ]
})
export class SharedModule { }
