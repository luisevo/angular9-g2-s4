import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MusicCardComponent } from './music-card/music-card.component';

@NgModule({
  declarations: [MusicCardComponent],
  exports: [MusicCardComponent],
  imports: [
    CommonModule
  ]
})
export class SpotifyComponentsModule { }
