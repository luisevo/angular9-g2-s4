import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Artist } from '../../models/artist.model';

@Component({
  selector: 'app-music-card',
  templateUrl: './music-card.component.html',
  styleUrls: ['./music-card.component.scss']
})
export class MusicCardComponent implements OnInit {
  @Input() name: string;
  @Input() img: string;
  @Input() artists: Artist[] = [];
  @Output() goArtist: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

}
