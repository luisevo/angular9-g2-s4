import { IArtist } from './artist.interface';
import { IImage } from './image.interface';

export interface IAlbum {
  artists: IArtist[];
  name: string;
  images: IImage[];
}
