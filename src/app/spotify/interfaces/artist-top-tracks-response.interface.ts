import { ITrack } from './track.interface';

export interface ArtistTopTracksResponse {
  tracks: ITrack[];
}
