import { IImage } from './image.interface';

export interface IArtist {
  id: string;
  name: string;
  images?: IImage[];
  external_urls?: {
    spotify: string
  };
}
