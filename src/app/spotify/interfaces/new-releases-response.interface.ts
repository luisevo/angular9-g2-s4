import { IAlbum } from './album.interface';

export interface NewReleasesResponse {
  albums: {
    items: IAlbum[]
  };
}
