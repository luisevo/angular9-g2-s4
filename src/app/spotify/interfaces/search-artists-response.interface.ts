import { IArtist } from './artist.interface';

export interface SearchArtistsResponse {
  artists: {
    items: IArtist[]
  };
}
