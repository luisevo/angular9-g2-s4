import { IArtist } from './artist.interface';
import { IAlbum } from './album.interface';

export interface ITrack {
  name: string;
  preview_url: string;
  uri: string;
  album: IAlbum;
}
