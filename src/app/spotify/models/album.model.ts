import { Artist } from './artist.model';
import { IAlbum } from '../interfaces/album.interface';

export class Album {

  name: string;
  imagen: string;
  artists: Artist[];

  constructor(data: IAlbum) {
    this.name = data.name;
    this.imagen = data.images && data.images.length ? data.images[0].url : '';
    this.artists = data.artists.map(iArtist => new Artist(iArtist));
  }

}
