import { IArtist } from '../interfaces/artist.interface';

export class Artist {
  id: string;
  name: string;
  image: string;
  pageUrl: string;

  constructor(data: IArtist) {
    this.id = data.id || '';
    this.name = data.name || '';
    this.image = data.images && data.images.length ? data.images[0].url : '';
    this.pageUrl = data.external_urls ? data.external_urls.spotify : '';
  }
}
