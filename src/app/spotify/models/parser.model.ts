import { NewReleasesResponse } from '../interfaces/new-releases-response.interface';
import { SearchArtistsResponse } from '../interfaces/search-artists-response.interface';

import { Album } from './album.model';
import { Artist } from './artist.model';
import { Track } from './track.model';
import { ArtistTopTracksResponse } from '../interfaces/artist-top-tracks-response.interface';

export class Parser {

  static newReleases(data: NewReleasesResponse): Album[] {
    if (data?.albums?.items) {
      return data.albums.items.map(iAlbum => new Album(iAlbum) );
    }

    return [];
  }

  static searchArtist(data: SearchArtistsResponse): Artist[] {
    if (data?.artists?.items) {
      return data.artists.items.map(iArtist => new Artist(iArtist) );
    }

    return [];
  }

  static artistTopTracks(data: ArtistTopTracksResponse): Track[] {
    if (data?.tracks) {
      return data.tracks.map(iTrack => new Track(iTrack) );
    }

    return [];
  }


}
