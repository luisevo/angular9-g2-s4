import { Album } from './album.model';
import { ITrack } from '../interfaces/track.interface';

export class Track {

  name: string;
  previewUrl: string;
  uri: string;
  album: Album;

  constructor(data: ITrack) {
    this.name = data.name;
    this.previewUrl = data.preview_url;
    this.uri = data.uri;
    this.album = new Album(data.album);
  }

}
