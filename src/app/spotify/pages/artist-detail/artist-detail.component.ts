import { Component, OnInit } from '@angular/core';
import { Artist } from '../../models/artist.model';
import { ApiService } from '../../services/api.service';
import { ActivatedRoute } from '@angular/router';
import { Track } from '../../models/track.model';

@Component({
  selector: 'app-artist-detail',
  templateUrl: './artist-detail.component.html',
  styleUrls: ['./artist-detail.component.scss']
})
export class ArtistDetailComponent implements OnInit {
  artist: Artist;
  tracks: Track[] = [];

  constructor(
    private api: ApiService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.paramMap.get('id');
    this.api.getArtist(id)
    .subscribe(
      artist => this.artist = artist,
      err => console.log(err)
    );

    this.api.getArtistTopTracks(id)
    .subscribe(
      tracks => this.tracks = tracks,
      err => console.log(err)
    );
  }

}
