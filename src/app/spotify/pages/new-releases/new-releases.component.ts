import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Album } from '../../models/album.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-releases',
  templateUrl: './new-releases.component.html',
  styleUrls: ['./new-releases.component.scss']
})
export class NewReleasesComponent implements OnInit {
  albums: Album[] = [];

  constructor(
    private api: ApiService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.api.getNewReleases()
    .subscribe(
      albums => this.albums = albums,
      err => console.log(err)
    );
  }

  goArtistDetail(id: string) {
    this.router.navigate(['/spotify/detalle-artista', id]);
  }


}
