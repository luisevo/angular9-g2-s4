import { Component } from '@angular/core';
import { Artist } from '../../models/artist.model';
import { ApiService } from '../../services/api.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-search-artist',
  templateUrl: './search-artist.component.html',
  styleUrls: ['./search-artist.component.scss']
})
export class SearchArtistComponent {
  artists: Artist[] = [];
  form: FormGroup;

  constructor(
    private api: ApiService,
    private fb: FormBuilder
  ) {
    this.form = this.fb.group({
      q: ['', Validators.required]
    });
  }

  search() {
    this.api.searchArtists(this.form.value.q)
    .subscribe(
      artists => this.artists = artists,
      err => console.log(err)
    );
  }

}
