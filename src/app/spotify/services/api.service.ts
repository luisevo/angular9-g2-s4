import { Injectable } from '@angular/core';
import { SpotifyServicesModule } from './services.module';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NewReleasesResponse } from '../interfaces/new-releases-response.interface';
import { map } from 'rxjs/operators';
import { Album } from '../models/album.model';
import { Parser } from '../models/parser.model';
import { SearchArtistsResponse } from '../interfaces/search-artists-response.interface';
import { Artist } from '../models/artist.model';
import { IArtist } from '../interfaces/artist.interface';
import { Track } from '../models/track.model';
import { ArtistTopTracksResponse } from '../interfaces/artist-top-tracks-response.interface';

@Injectable({
  providedIn: SpotifyServicesModule
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getNewReleases(): Observable<Album[]> {

    const headers = {
      Authorization: 'Bearer BQC7t750s7XUzSKFDR7F7_5PIhaPURVYazp1wkLIEv04cQ8Dr-oO5-q5P0Oe2seg3lY-cvwJHmA-pUNh5PI'
    };

    return this.http.get<NewReleasesResponse>('https://api.spotify.com/v1/browse/new-releases?limit=20', { headers })
    .pipe(
      map((originalResponse) => Parser.newReleases(originalResponse))
    );
  }

  searchArtists(q: string): Observable<Artist[]> {

    const headers = {
      Authorization: 'Bearer BQC7t750s7XUzSKFDR7F7_5PIhaPURVYazp1wkLIEv04cQ8Dr-oO5-q5P0Oe2seg3lY-cvwJHmA-pUNh5PI'
    };

    return this.http.get<SearchArtistsResponse>(`https://api.spotify.com/v1/search?q=${q}&type=artist&limit=15`, { headers })
    .pipe(
      map((originalResponse) => Parser.searchArtist(originalResponse))
    );
  }

  getArtist(id: string): Observable<Artist> {
    const headers = {
      Authorization: 'Bearer BQC7t750s7XUzSKFDR7F7_5PIhaPURVYazp1wkLIEv04cQ8Dr-oO5-q5P0Oe2seg3lY-cvwJHmA-pUNh5PI'
    };

    return this.http.get<IArtist>(`https://api.spotify.com/v1/artists/${id}`, { headers })
    .pipe(
      map((iArtist) => new Artist(iArtist))
    );
  }

  getArtistTopTracks(id: string): Observable<Track[]> {
    const headers = {
      Authorization: 'Bearer BQC7t750s7XUzSKFDR7F7_5PIhaPURVYazp1wkLIEv04cQ8Dr-oO5-q5P0Oe2seg3lY-cvwJHmA-pUNh5PI'
    };

    return this.http.get<ArtistTopTracksResponse>(`https://api.spotify.com/v1/artists/${id}/top-tracks?country=us`, { headers })
    .pipe(
      map((originalResponse) => Parser.artistTopTracks(originalResponse))
    );
  }

}
