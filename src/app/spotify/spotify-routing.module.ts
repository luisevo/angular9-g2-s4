import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SpotifyComponent } from './spotify.component';
import { NewReleasesComponent } from './pages/new-releases/new-releases.component';
import { ArtistDetailComponent } from './pages/artist-detail/artist-detail.component';
import { SearchArtistComponent } from './pages/search-artist/search-artist.component';

const routes: Routes = [
  // spotify
  {
    path: '',
    component: SpotifyComponent,
    children: [
      { path: '', redirectTo: 'nuevos-lanzamientos', pathMatch: 'full' },
      { path: 'nuevos-lanzamientos', component: NewReleasesComponent },
      { path: 'buscar-artista', component: SearchArtistComponent },
      { path: 'detalle-artista/:id', component: ArtistDetailComponent },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SpotifyRoutingModule { }
