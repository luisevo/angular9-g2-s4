import { Component, OnInit } from '@angular/core';
import { HeaderOption } from '../shared/components/header/header.interfaces';

@Component({
  selector: 'app-spotify',
  templateUrl: './spotify.component.html',
  styleUrls: ['./spotify.component.scss']
})
export class SpotifyComponent implements OnInit {
  title = 'Spotify App';
  mainUrl = '/spotify';
  headerOptions: HeaderOption[] = [
    { url: '/spotify/buscar-artista', title: 'Buscar' }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
