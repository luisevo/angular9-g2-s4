import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SpotifyRoutingModule } from './spotify-routing.module';
import { SpotifyComponent } from './spotify.component';
import { NewReleasesComponent } from './pages/new-releases/new-releases.component';
import { SearchArtistComponent } from './pages/search-artist/search-artist.component';
import { ArtistDetailComponent } from './pages/artist-detail/artist-detail.component';
import { SpotifyComponentsModule } from './components/components.module';
import { SharedComponentsModule } from '../shared/components/components.module';
import { SpotifyServicesModule } from './services/services.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    SpotifyComponent,
    NewReleasesComponent,
    SearchArtistComponent,
    ArtistDetailComponent
  ],
  imports: [
    CommonModule,
    SpotifyRoutingModule,
    SpotifyComponentsModule,
    SpotifyServicesModule,
    SharedComponentsModule,
    ReactiveFormsModule,
  ]
})
export class SpotifyModule { }
